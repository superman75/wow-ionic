import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NoticeuserviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-noticeuserview',
  templateUrl: 'noticeuserview.html',
})
export class NoticeuserviewPage {

  notice : any;	

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.notice = this.navParams.get('notice');
    console.log(this.notice);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoticeuserviewPage');
  }

  backPage() {
    this.navCtrl.pop();
  }
}
